import os
import sys

from PyPDF2 import PdfFileMerger

filename = ""

if len(sys.argv) == 1:
    filename = "mergedpdf.pdf"
    print("Current Filename Will Be... " + filename)
else:
    filename = sys.argv[1]
    index = filename.find('.pdf')

    if index == -1:
        filename = filename + ".pdf"
    print("Current Filename Will Be... " + filename)


f = open("data.txt", "r")

pdfs = []
line = f.readline()

while line:
    index = line.find('''\n''')

    if index != -1:
        pdfs.append(line[0:index])
    else:
        pdfs.append(line)
    line = f.readline()

merger = PdfFileMerger(strict=False)

for pdf in pdfs:
    merger.append(pdf)
    print(pdf)

merger.write(filename)

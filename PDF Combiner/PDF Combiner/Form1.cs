﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PDF_Combiner
{
    public partial class Form1 : Form
    {
        private List<File> files;
        private int selectedIndex;
        private String currentDirectory;

        public Form1()
        {
            files = new List<File>();
            selectedIndex = 0;
            InitializeComponent();
            currentDirectory = System.IO.Directory.GetCurrentDirectory();
        }

        private void butAdd_Click(object sender, EventArgs e)
        {
            string path = "none";
            OpenFileDialog file = new OpenFileDialog();
            
            if (file.ShowDialog() == DialogResult.OK)
            {
                path = file.FileName;
            }

            if (path != "none")
            {
                //files.Add(new File(path));
                list.Items.Add(path);
                //lblFile.Text = path;
            }
            
        }

        private void butDelete_Click(object sender, EventArgs e)
        {
            int selected = list.FocusedItem.Index;

            list.Items.RemoveAt(selected);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem line in list.Items)
            {
                files.Add(new File(line.Text));
            }

            PythonHandler p = new PythonHandler();
            
            String current = currentDirectory + "\\data.txt" ;

            p.run_cmd(txtName.Text, files);
            
            txtName.Text = "";
            files.Clear();
            list.Clear();
        }

        private void btnUp_Click(object sender, EventArgs e)
        {
            selectedIndex = list.FocusedItem.Index;

            if (selectedIndex > 0)
            {
                String indexed = list.FocusedItem.Text;
                String newLocation = list.Items[selectedIndex - 1].Text;

                list.Items[selectedIndex - 1].Text = indexed; // Should be new selected index
                list.Items[selectedIndex].Text = newLocation;
                list.Items[selectedIndex - 1].Focused = true;
                list.Items[selectedIndex - 1].Selected = true;

                list.Items[selectedIndex].Selected = false;
                selectedIndex = selectedIndex - 1;
            }
            else
            {
                Console.WriteLine("Cant Go Further");
            }
            
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            selectedIndex = list.FocusedItem.Index;

            if (list.Items.Count - 1> selectedIndex)
            {
                String indexed = list.FocusedItem.Text;
                String newLocation = list.Items[selectedIndex + 1].Text;

                list.Items[selectedIndex + 1].Text = indexed; // Should be new selected index
                list.Items[selectedIndex].Text = newLocation;

                list.Items[selectedIndex + 1].Focused = true;
                list.Items[selectedIndex + 1].Selected = true;
                list.Items[selectedIndex].Selected = false;

                selectedIndex = selectedIndex + 1;

            }
            else
            {
                Console.WriteLine("Cant Go Further");
            }
        }


    }
}

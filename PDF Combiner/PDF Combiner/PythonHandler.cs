﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Collections;
using System.Windows.Forms;

namespace PDF_Combiner
{
    class PythonHandler
    {
        private String currentDirectory;
        public PythonHandler()
        {
            currentDirectory = System.IO.Directory.GetCurrentDirectory();
        }

        public void run_cmd(string filename, List<File> data)
        {
            Process p = new Process(); // create process to run the python program
            String filePath = @System.IO.Path.GetDirectoryName(Application.ExecutablePath) + @"\data.txt";
            String pythonFilename = @System.IO.Path.GetDirectoryName(Application.ExecutablePath) + @"\combiner.py";
            System.IO.StreamWriter file = new System.IO.StreamWriter(filePath);

            foreach (File f in data)
            {
                file.WriteLine(f.getFile());
            }

            file.Close();

            string[] scripts = { "combiner.py" };

            //Gets File Directory for desired script
            string fileName = currentDirectory + @"\" +scripts[0];
            Console.WriteLine(fileName);

            //Process Start Info
            p.StartInfo.FileName = "python.exe"; //Python.exe location
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false; // ensures you can read stdout
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.Arguments = string.Format("\"{0}\"", pythonFilename) + " " + filename;
            p.Start();

            StreamReader s = p.StandardOutput;

            p.WaitForExit();


        }


        public String price_grabber(string parameter1)
        {
            Process p = new Process(); // create process to run the python program
            string[] scripts = { "priceChecker.py" };

            //Gets File Directory for desired script
            string fileName = currentDirectory + scripts[0];

            //Process Start Info
            p.StartInfo.FileName = "python.exe"; //Python.exe location
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false; // ensures you can read stdout
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.Arguments = string.Format("\"{0}\"", fileName) + " " + parameter1;
            p.Start();

            StreamReader s = p.StandardOutput;
            String data = s.ReadToEnd();
            Console.WriteLine(data);
            p.WaitForExit();

            return data;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDF_Combiner
{
    class File
    {
        private String FILENAME;

        public File(String name)
        {
            FILENAME = name;
        }

        public String getFile()
        {
            return FILENAME;
        }
    }
}
